# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=1000
setopt extendedglob
unsetopt autocd beep nomatch
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/simon/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

export PATH=$PATH:$HOME/.local/bin/

autoload -U colors && colors

PROMPT="%F{8}%B<%b%n@%m%B>%b %F{3}%~ %F{8}%#%f "
RPROMPT="%F{8}[%h,%T]%f"

export EDITOR="emacsclient -c"
export VISUAL="emacsclient -c"
export BROWSER="chromium"
