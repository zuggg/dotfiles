;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Emacs configuration
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Enable debug during loading.
(setq debug-on-error t)

;;; Store additional config in a 'lisp' subfolder and add it to the load path so
;;; that `require' can find the files.  This must be done before moving
;;; `user-emacs-directory'.
(add-to-list 'load-path (expand-file-name "lisp/" user-emacs-directory))

;;; Move user-emacs-directory so that user files don't mix with cache files.
(setq user-emacs-directory "~/.cache/emacs/")

;;; Local plugin folder for quick install. All files in this folder will be
;;; accessible to Emacs config.  This is done to separate the versioned config
;;; files from the external packages.
(add-to-list 'load-path "~/.emacs.d/local")

(when (require 'package nil t)
  (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
  (add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
  (setq package-user-dir (expand-file-name "elpa/" package-user-dir))
  (package-initialize))

;; load main config
(require 'main)
;(require 'functions)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Magit
(when (fboundp 'magit-status)
  (global-set-key (kbd "C-x g") 'magit-status))

;;; Python
(add-hook 'python-mode-hook (lambda () (require 'init-python)))

;;; Helm
(when (require 'helm-config nil t) (require 'init-helm))

;;; Bibtex
;; (setq bibtex-entry-format '(opts-or-alts required-fields numerical-fields whitespace realign last-comma delimiters braces sort-fields))
;; (setq bibtex-field-delimiters 'double-quotes)
;; (add-hook 'bibtex-mode-hook 'turn-off-indent-tabs)

;;; TeX / LaTeX / Texinfo
;; (with-eval-after-load 'tex-mode (require 'init-tex))
;; (with-eval-after-load 'texinfo (require 'init-texinfo))
;; ;;; LaTeX is defined in the same file as TeX. To separate the loading, we add it
;; ;;; to the hook.
;; (add-hook 'latex-mode-hook (lambda () (require 'init-latex)))
;; ;; (nconc package-selected-packages '(latex-math-preview))

;;; Lisp
;;(dolist (hook '(lisp-mode-hook emacs-lisp-mode-hook))
;;  (add-hook hook 'turn-on-fmt-before-save))
;;  (add-hook hook 'turn-on-tab-width-to-8)) ; Because some existing code uses tabs.
;;  (add-hook hook 'turn-off-indent-tabs)) ; Should not use tabs.
;; (define-key lisp-mode-shared-map "\M-." 'find-symbol-at-point)
;;; Common LISP
;; (setq inferior-lisp-program "clisp")

;;; Matlab / Octave
;; (add-to-list 'auto-mode-alist '("\\.m\\'" . octave-mode)) ; matlab
;; (defun octave-set-comment-start ()
;;   "Set comment character to '%' to be Matlab-compatible."
;;   (set (make-local-variable 'comment-start) "% "))
;; (add-hook 'octave-mode-hook 'octave-set-comment-start)
 
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages (quote (helm magit elpy))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;; Disable debug now we are done with init.
;;(setq debug-on-error nil)
