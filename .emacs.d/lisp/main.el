;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Main options
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; UI tweaks

;;; Minimal UI. Run early to hide it as soon as possible.
(setq inhibit-startup-screen t)
(menu-bar-mode -1)

;;; `tool-bar-mode' and `scroll-bar-mode' might not be compiled in.
(when (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
;;; In some cases, Emacs can still decide by itself to use graphical boxes.
;;; Force on using the minibuffer instead.
(setq use-dialog-box nil)

;;; Show cursor position within line.
(column-number-mode 1)
(when (version<= "26.0.50" emacs-version )
  (global-display-line-numbers-mode))

;;; load a default theme and font
(load-theme 'wombat)

;; For some reason both of these conditions are not verified at startup, but one
;; might want to use them to avoid errors.
;(when (find-font (font-spec :name "DejaVu Sans Mono"))
;(when (member "DejaVu Sans Mono" (font-family-list)) 
(add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-13"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Editing

;;; Default major mode should be simple, so that big files do not slow emacs
;;; down.
(setq-default major-mode 'fundamental-mode)

;;; Show matching parenthesis
(show-paren-mode 1)
;;; By default, there’s a small delay before showing a matching parenthesis. Set
;;; it to 0 to deactivate.
(setq show-paren-delay 0
      show-paren-when-point-inside-paren t)

;;; Auto-fill. Line wrapping is not great when coding, so we do not
;;; want it on by default. M-q is bound to fill-paragraph, by default.
;;; Entering latex-mode runs the hook text-mode-hook, so that we may
;;; want this to be disabled globally.
;;; (add-hook 'text-mode-hook 'turn-on-auto-fill)
(setq-default fill-column 80)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Misc

(setq
 ;; Disable backups
 make-backup-files nil
 
 ;; Disable autosave features.
 auto-save-default nil
 auto-save-list-file-prefix nil
 )

;;; Disable suspend key
(global-unset-key (kbd "C-z"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'main)
